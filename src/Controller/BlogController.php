<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use App\Repository\ArticleRepository;
use App\Repository\CategoryRepository;
use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\Category;
use App\Form\ArticleType;
use App\Form\CommentType;
use App\Form\CategoryType;


class BlogController extends AbstractController
{
    /**
     * @Route("/blog", name="blog")
     */
    public function index(ArticleRepository $repo): Response
    {
        //$repo = $this->getDoctrine()->getRepository(Article::class);
        $articles = $repo->findAll();
        return $this->render('blog/index.html.twig', [
            'controller_name' => 'BlogController',
            'articles' => $articles
        ]);
    }

    /**
     * @Route("/", name="home")
     */
    public function home(){
        return $this->render('blog/home.html.twig', [
            'title'=>"bienvenue",
            'age'=>31
        ]);
    }

    /**
     * @Route("/blog/new", name="blog_create")
     * @Route("/blog/{id}/edit", name="blog_edit")
     */
    public function form(Article $article = null, Request $request, EntityManagerInterface $manager){
        
        if(!$article){
            $article = new Article();
        }

        $form = $this->createForm(ArticleType::class, $article);               
        $form->handleRequest($request);

        //dump($article);

        if($form->isSubmitted() && $form->isValid()){
            if( !$article->getId() ){
                $article->setCreatedAt(new \DateTime());
            }
            
            $manager->persist($article);
            $manager->flush();

            return $this->redirectToRoute('blog_show', ['id'=> $article->getId()]);

        }

        return $this->render('blog/create.html.twig', [
            'formArticle'=>$form->createView(),
            'editMode' => $article->getId() !== null
        ]);
    }

    /**
     * @Route("/blog/{id}", name="blog_show")
     */
    public function show(Article $article, Request $request, EntityManagerInterface $manager){
        //$repo = $this->getDoctrine()->getRepository(Article::class);
        //$article = $repo->find($id);

        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if($form->isSubmitted()&&$form->isValid()){

            $comment->setCreatedAt(new \DateTime())
                    ->setArticle($article);

            $manager->persist($comment);
            $manager->flush();

            return $this->redirectToRoute('blog_show', ['id'=> $article->getId()]);
;        }

        return $this->render('blog/show.html.twig',[
            'article' => $article,
            'commentForm' => $form->createView()
        ]);
        
    }

    /**
     * @Route("/categorie", name="category")
     */
    public function category(CategoryRepository $repo): Response
    {
        $categories = $repo->findAll();
        return $this->render('blog/indexCat.html.twig', [
            'controller_name' => 'BlogController',
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/categorie/new", name="category_create")
     * @Route("/categorie/{id}/edit", name="category_edit")
     */
    public function formCategory(Category $category = null, Request $request, EntityManagerInterface $manager){
        
        if(!$category){
            $category = new Category();
        }

        $form = $this->createForm(CategoryType::class, $category);               
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            
            $manager->persist($category);
            $manager->flush();

            return $this->redirectToRoute('category');

        }

        return $this->render('blog/category.html.twig', [
            'formCategory'=>$form->createView(),
            'editMode' => $category->getId() !== null
        ]);
    }

    

}
